import Vue from 'vue'
import App from './App.vue'
import VueScrollactive from 'vue-scrollactive'
import '@/assets/css/tailwind.css'

Vue.config.productionTip = false

Vue.use(VueScrollactive)

new Vue({
  render: h => h(App),
}).$mount('#app')
